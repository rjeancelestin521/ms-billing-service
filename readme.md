# ms-billing-service

ms-billing-service est un API microservice

## Environnement de développement

### Pré-requis

* Java 11
* maven
* lombok
* Spring Data JPA
* Spring Web
* H2 Database
* Eureka Discovery Client
* Map Struct
* Spring doc openapi-ui
* openfeign

## Nom microservice
```bash
BILLING-SERVICE
```

## Fichier de configuration application.properties
```bash
server.port=8083
spring.application.name=BILLING-SERVICE
spring.h2.console.enabled=true
spring.cloud.discovery.enabled=false
spring.datasource.url=jdbc:h2:mem:billing-db
```
## Accès aux documentations (api)
```bash
http://localhost:8083/v3/api-docs
http://localhost:8083/swagger-ui.html
```