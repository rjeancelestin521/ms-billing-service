package mg.rjc.msbillingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsBillingServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsBillingServiceApplication.class, args);
    }

}
