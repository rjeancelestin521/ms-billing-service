package mg.rjc.msbillingservice.openfeign;

import mg.rjc.msbillingservice.models.Customer;

import java.util.List;

public interface CustomerServiceRestClient {
    Customer customerById(String id);
    List<Customer> customers();
}
