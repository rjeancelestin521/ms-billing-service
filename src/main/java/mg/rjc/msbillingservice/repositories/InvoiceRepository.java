package mg.rjc.msbillingservice.repositories;

import mg.rjc.msbillingservice.entities.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceRepository extends JpaRepository<Invoice, String> {
}
